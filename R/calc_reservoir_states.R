#' Flow at reservoir intakes and release
#'
#' @param DS Compensation flow returned by [choose_DSn_DSx]
#' @inheritParams range_hard_constraints
#'
#' @inherit calcRBD return return
#' @export
#'
calc_reservoir_states <- function(DS, Qnat, Vreservoirs, constraints, cfg) {
  l <- lapply(names(DS), function(lake) {
    DS1 <- DS[lake]
    cnst <- constraints[[lake]]
    # Compute release
    if(DS1 < 0) {
      Qrelease <- cnst$out$min
    } else {
      Qrelease <- DS1
    }
    Qrelease <- setNames(obj = Qrelease + cnst$replenish,
                         nm = paste0("LAC_", lake))

    # Compute intakes
    if (!is.null(cnst$`in`)) {
      if (DS1 > 0) {
        QintakeTot <- 0
      } else {
        QintakeTot <- - DS1
      }
      In_Ids <- names(cnst$`in`)
      Qavail <- sapply(Qnat[In_Ids] - unlist(cnst$res[In_Ids]), max, 0)
      if (sum(Qavail) > 0) {
        QnatRatio <- Qavail / sum(Qavail)
      } else {
        QnatRatio <- Qavail
      }
      Qintake <- QnatRatio * QintakeTot

    } else {
      Qintake <- NULL
    }

    # Update reservoir Volume
    Vreservoir <- Vreservoirs[lake] * 1E6
    if (!is.null(cnst$impluvium)) {
      Vreservoir <- Vreservoir + sum(Qnat[cnst$impluvium])
    }
    Vreservoir <- setNames(obj = (Vreservoir + sum(Qintake) - Qrelease),
                           nm = paste0(lake, ".Vreservoir"))
    list(Qintake = Qintake, Qrelease = Qrelease, Vreservoir = Vreservoir)
  })
  RS <- flatten_reservoir_state(l)
  attr(RS, "risk") <- attr(DS, "risk")
  return(RS)
}

flatten_reservoir_state <- function(lRS) {
  RS <- unlist(lRS)
  names(RS) <- gsub("Qintake\\.|Qrelease\\.|Vreservoir\\.", "", names(RS))
  return(RS)
}
