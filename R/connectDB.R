#' Environnement to record current database connections
#' @noRd
DB_ENV <- new.env()

#' Manage connection to RVGEST SQlite database
#'
#' - `connectDB` creates the connection to the database if it doesn't exist.
#' The connection is automatically closed at session end or on package unload.
#' - `disconnectDB` manually disconnect the database
#' - `getDBhost` returns the path of the SQlite database file or ":memory:" for
#' an in memory database
#'
#' @details
#' Connection type is set by `cfg$database$mode` which can takes following values:
#'
#' - "package": path inside the installed package (correspond at path inside "inst" in source)
#' - "file": local path to a file (empty value by default uses a temporary file)
#' - "temp": name of a file stored in the R session temporary folder
#' - "memory": in memory database
#' - any other value can be used if the corresponding key exists in `cfg$database`
#' and will be directly used by [DBI::dbConnect] as second argument
#'
#' @template param_cfg
#' @param reset [logical], `TRUE` to force new connection to the database
#' @param host [character], location of the database (See details)
#' @param e Environment where the connection is recorded
#'
#' @return `connectDB` returns a DBI connection, `disconnectDB` return `TRUE` if
#' an existing connection has been closed, `getDBhost` returns a `character` with
#' the path of the database.
#'
#' @export
#'
#' @rdname connectDB
#'
#' @examples
#' # Connect to an empty database in memory
#' cfg <- loadConfig()
#' cfg$database$mode <- "memory"
#' con <- connectDB(cfg)
#' disconnectDB()
#'
#' # Connect to an empty database on a temporary file
#' cfg <- loadConfig()
#' cfg$database$mode <- "file"
#' # cfg$database$file is empty by default and produce the same result as below:
#' cfg$database$file <- tempfile()
#' con <- connectDB(cfg)
#' disconnectDB()
#'
#' # Connect to the test database
#' cfg <- loadConfig()
#' cfg$database$mode <- "test"
#' con <- connectDB(cfg)
#' disconnectDB()
#'
connectDB <- function(cfg = loadConfig(),
                      host = getDBhost(cfg, RDS),
                      reset = FALSE,
                      RDS = FALSE,
                      e = DB_ENV) {

  if (reset) disconnectDB(e)
  if (RDS) {
    disconnectDB(e)
    e$con <- readRDS(host)
    class(e$con) <- c("RvgestDB", class(e$con))
    logger::log_trace("RDS database loaded from: ", host)
  } else {
    if (is.null(e$con) || !DBI::dbIsValid(e$con)) {
      e$con <- DBI::dbConnect(RSQLite::SQLite(), host, extended_types = TRUE)
      logger::log_trace("New connection to database: ", host)
    } else {
      logger::log_trace("Already connected to database: ",
                        DBI::dbGetInfo(e$con)$dbname)
    }
  }
  return(e$con)
}

#' @rdname connectDB
#' @export
getDBhost <- function(cfg = loadConfig(), RDS, id_ruleset = cfg$CDF$ruleset$default) {
  stopifnot(cfg$database$mode %in% names(cfg$database)[names(cfg$database) != "mode"])

  host <- cfg$database[[cfg$database$mode]]
  if (RDS && cfg$database$mode == "package") {
    host <- getDataPath(cfg$CDF$path,
                        sprintf("%s.ruleset%i.RDS", host, id_ruleset),
                        cfg = cfg)
  } else if (!RDS) {
    if (cfg$database$mode == "package") {
      host <- install_CDF_database(cfg)
    } else if (cfg$database$mode == "temp") {
      host <- file.path(tempdir(), cfg$database$temp)
    } else if (cfg$database$mode == "test") {
      host <- getDataPath(cfg$CDF$path, host, cfg = cfg)
    }
  } else {
    stop("RDS =  TRUE only handled for cfg$database$mode == package")
  }
  return(host)
}


#' @rdname connectDB
#' @export
disconnectDB <- function(e = DB_ENV) {
  conn_name <- "con"
  # Want to be as defensive as possible, so if there is no connection, we don't want to test it
  if (conn_name %in% ls(e)) {
    conn <- get(conn_name, envir = e)
    # If connection has closed for any other reason, we don't want the function to error
    if (inherits(e$con, "SQLiteConnection") && DBI::dbIsValid(conn)) {
      DBI::dbDisconnect(conn)
    }
    rm(list=conn_name, envir = DB_ENV)
    return(TRUE)
  }
  return(FALSE)
}


.onLoad <- function(libname, pkgname) {
  reg.finalizer(
    e = DB_ENV,
    f = disconnectDB,
    onexit = TRUE
  )
}

.onUnload <- function(libpath) {
  disconnectDB(DB_ENV)
}




