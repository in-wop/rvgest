#' Characteristics of the 4 lakes managed by Seine Grands Lacs
#'
#' @format a [data.frame] with 3 columns:
#'
#' - "name" the name of the river on which the lake is implemented ([character])
#' - "min" the minimum lake storage in Mm3 ([numeric])
#' - "max" the maximum lake storage in Mm3 ([numeric])
#'
#' @source \url{https://www.seinegrandslacs.fr/quatre-lacs-reservoirs-au-coeur-dun-bassin}
#' @export
"lakes"

#' Rule sets used in the reservoir managements
#'
#' @format a [list] with 2 items:
#'
#' - "constraints" [character] description of the constraints apply on the management of the reservoir
#' - "rules" [character] description of rule set selections depending on which constraints are applied
#'
#' @source Dehay, F., 2012. Etude de l’impact du changement climatique sur la gestion des lacs-réservoirs de la Seine (other). Diplôme d’ingénieur de l’ENGEES ,Strasbourg. \url{https://hal.inrae.fr/hal-02597326}
#' @export
"rulesets"

#' Objectives of reservoir management on the Seine River
#'
#' @format a [data.frame] with 5 columns:
#'
#' - "station" the id of the station ([character])
#' - "description" the description of the objective with the complete name of the
#' station and the description of the threshold
#' - "flood" a [logical] which is `TRUE` for a flood objective and `FALSE` for a drought objective
#' - "level" a [character] representing the severity of the threshold: "l1" to "l4" for low flow thresholds and "h1" to "h3" for high flow thresholds
#' - "lakes" a [list] which contains a [data.frame] with informations concerning the lakes (See [lakes])
#' - "hydro2" the code used in seinebasin2 for the station (hydro2 code if exists)
#'
#' @source Dorchies, D., Thirel, G., Jay-Allemand, M., Chauveau, M., Dehay, F., Bourgin, P.-Y., Perrin, C., Jost, C., Rizzoli, J.-L., Demerliac, S., Thépot, R., 2014. Climate change impacts on multi-objective reservoir management: case study on the Seine River basin, France. International Journal of River Basin Management 12, 265–283. \url{https://doi.org/10.1080/15715124.2013.865636}
#' @export
"objectives"

#' #' Objectives by lake
#' #'
#' #' @source Extracted from [objectives] table
#' #'
#' #' @format a [data.frame] with 2 columns:
#' #' - `id_objective` [integer]
#' #' - `lake` [character]
#' #'
#' "lakes_objectives"

#' Color map from IPCC recommendation for temperature seqential color map
#' @format A [character] matrix with 256 rows and 2 columns containing colors in
#' HTML format (column one for droughts and two for floods).
#' @source https://github.com/IPCC-WG1/colormaps/tree/master/continuous_colormaps_rgb_0-1
"heat_colormap"
