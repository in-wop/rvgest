#' Get volume storage curves
#'
#' @param con connect to the database
#' @param selected_curves selected curves (select the year of the curves)
#' @param theoretical [data.frame] returned by the function [getObjectiveStorageCurve]
#'
#' @return A [data.frame] with the following columns:
#'   - 'lake' [character]: name of the lake
#'   - 'mmjj' [character]: calendar day (month followed by the day both formatted by two digits)
#'   - 'type' [character]: theoretical and observed
#'   - 'V' [numeric]: Volume storage associated to the risk probability
#'   - 'julian' [numeric]: julian day of the year (number of days since 1st January)
#'
#' @export
#'
#' @importFrom stats complete.cases
#' @importFrom dplyr tbl filter collect
#' @examples
#' cfg <- loadConfig()
#' cfg$database$mode <- "test"
#' con <- initDB(cfg)
#' getItemCurves(con = con, selected_curves = c("2022", "2021", "theoretical"))
#'
getItemCurves <- function(con, selected_curves, theoretical = getObjectiveStorageCurves()){

  vols <- tbl(con, "volume")
  vol <- vols %>% collect()
  vol <- vol[!is.na(vol$date), ]
  vol <- vol %>% filter(format(date, format = "%Y") %in% {{selected_curves}})

  mmjj <- as.factor(format(vol$date, format = "%m%d"))
  julian <- as.integer(as.factor(mmjj))
  type <- format(vol$date, format = "%Y")

  if(length(unique(type))>5){type <- "observed"}

  V <- as.numeric(vol$V /1000)
  lake  <- as.character(lakes$name[vol$id_lake])
  df <- data.frame(lake, julian, mmjj, V, type)
  df <- df[complete.cases(df), ] # remove the value NA
  if ("theoretical" %in% selected_curves) {
    df <- rbind(df, theoretical)
  }
  return(df)
}
