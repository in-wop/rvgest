#' plot of instant risk for all objective
#'
#' @inheritParams calcInstantRisk
#'
#'
#' @export

#' @import ggplot2
#' @importFrom stats reorder
#'
plotInstantRisk <- function(con, ruleset, date, id_rcps, id_periods, storages, n = 10L) {
  dfInstantRisk <- calcInstantRisk(con, ruleset, date, id_rcps, id_periods, storages, n = n)
  if (nrow(dfInstantRisk) > 0) {
    levels <- c(paste0("h", seq(3, 1, -1)), paste0("l", seq(4)))
    level_palette <- rev(RColorBrewer::brewer.pal(n = length(levels), name = "RdYlBu"))
    names(level_palette) <- levels
    ggplot(dfInstantRisk, aes(x = reorder(description, prob), y = prob)) +
      geom_col(aes(fill = factor(level, levels = levels))) +
      geom_text(aes(label = scales::percent_format(accuracy = 0.1)(prob)),
                size = 3, position = position_stack(vjust = 0.5)) +
      scale_y_continuous(name = "Failure risk",
                         labels = scales::percent_format(accuracy = 1)) +
      scale_x_discrete(name = "Objectives downstream the reservoirs") +
      scale_fill_manual(values = level_palette,
                        labels = levels,
                        drop = FALSE,
                        name = "Objectives") +
      coord_flip()
  } else {
    ggplot() +
      theme_void() +
      geom_text(aes(0,0,label='No objectives are at risk'))
  }
}
