#' Read RDS files produced from VGEST optimisations
#'
#' @param id_ruleset [integer] id of rule set of the reservoir
#' @param id_rcp [integer] id historical or rcp45 and rcp85
#' @param fileRds file rds to read
#' @param cfg a config object. Configuration to use rvgest
#' @param path_rcp [list] output of the fonction [getRcpScenariosPaths]
#'
#' @return A [data.frame]
#' @export

readVobj <- function(id_ruleset,
                     id_rcp,
                     fileRds,
                     cfg = loadConfig(),
                     path_rcp = getRcpScenariosPaths(cfg = cfg)) {

  isRcp <- grepl("rcp", names(cfg$drias$rcp)[id_rcp])
  scenarios <- gsub("/", "_", cfg$drias$scenarios)

  if (!isRcp) {
    dfRDS <- readRDS(
      getDataPath(cfg$Vobj$path,
                  path_rcp[[id_rcp]],
                  paste0("ruleset_", id_ruleset),
                  fileRds)
    )
  } else{
    lRDS <- lapply(seq_along(scenarios), function(id_scenario) {
      readRDS(
        getDataPath(cfg$Vobj$path,
                    path_rcp[[id_rcp]][id_scenario],
                    paste0("ruleset_", id_ruleset),
                    fileRds))
    })
    dfRDS <- do.call(rbind, lRDS)
  }
  return(dfRDS)
}
