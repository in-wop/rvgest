#' Compute maximum risk for one reservoir
#'
#' @inherit allocate_reactive_decision_range
#' @rdname reservoir_risk_max
#' @export
#'
reservoir_risk_max <- function(con,
                               ruleset,
                               j,
                               id_rcps,
                               id_periods,
                               Vreservoirs,
                               lake,
                               id_objectives) {
  id_objectives_lake <- lakes_objectives %>%
    filter(id_objective %in% id_objectives,
           lake == .env$lake) %>% pull(id_objective)
  probs <- sapply(id_objectives_lake, function(i) {
    sapply(
      lake,
      calcIR_lake,
      con = con,
      ruleset = ruleset,
      j = j,
      id_rcps = id_rcps,
      id_periods = id_periods,
      storages = Vreservoirs,
      id_objective = i,
      storage_capacity = TRUE
    )
  })
  return(max(probs))
}
