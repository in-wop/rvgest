save_isofrequency <- function(x, freq, result.dir = "database", output.dir = "output") {
  dir.create(output.dir, showWarnings = FALSE)
  file <- file.path(
    output.dir, paste0(
      paste("isofreq", x$station, x$level, x$threshold, sep = "_"),
      ".png")
  )
  cat("Plotting", file, "...")
  png(file = file,
    width = 16, height = 10, units = "cm", res = 200
  )
  plot_isofrequency(x, freq, result.dir)
  dev.off()
  cat(" OK\n")
}