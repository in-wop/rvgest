#' Prepare, run and store results of one or several VGEST instances
#'
#' @param ... other parameter passed to the method [vgest_write_batch]
#'
#' @return [NULL]
#' @export
#' @rdname vgest_run_store
#'
vgest_run_store <- function(x, ...) {
  UseMethod("vgest_run_store", x)
}


#'
#' @inheritParams vgest_write_batch
#' @param x See argument station
#' @param result.dir path for storing the result of vgest run. The result is stored in a
#' subfolder named high or low (depending on \code{bFlood}) followed by the threshold
#'
#' @export
#' @rdname vgest_run_store
#'
vgest_run_store.character <- function(x, reservoirRuleSet, networkSet,
                            Qfile = "Q_OBS_OBS.txt", startDate, endDate,
                            bFlood, threshold,
                            distributionType, distributionOption = NULL,
                            vgest_path = file.path(tempdir(), "vgest"),
                            objective_file = "BATCH",
                            formatResult = 1,
                            result.dir = file.path(tempdir(), "database"),
                            ...) {
  sLowHigh <- c("low", "high")
  station <- x
  cat("Run VGEST for configuration: ", station, Qfile, sLowHigh[bFlood + 1], threshold, "...")
  vgest_write_batch(reservoirRuleSet, networkSet, Qfile, startDate, endDate,
                    station, bFlood, threshold, distributionType, distributionOption,
                    vgest_path, objective_file, formatResult, ...)
  vgest_run(vgest_path)
  sTo <- file.path(result.dir, station, paste0(sLowHigh[bFlood + 1], "_", threshold))
  cat(" Store files to", sTo)
  dir.create(sTo, showWarnings = FALSE, recursive = TRUE)
  file.remove(list.files(path = sTo, full.names = TRUE))
  if(!all(file.copy(
    list.files(file.path(vgest_path, "RESULTAT"), full.names = TRUE),
    sTo, overwrite = TRUE
  ))) stop("Error copying Vojb files from VGEST to Irmara")
  cat(" - OK\n")
}

#' @param x row(s) of a [data.frame] provided by [objectives]
#'
#' @export
#' @rdname vgest_run_store
#'
#' @examples
#' \dontrun{
#' # Example with `vgest_run_store.Objectives`
#' # Running vgest for:
#' # - the first objective returned by `objectives`
#' # - the first configuration of reservoir rules
#' # - the first configuration of network
#' # - the naturalized hydrological flows of the file located in DONNEES/Q_NAT_1900-2009.txt
#' # - doing the optimization on the period between 01/01/1900 and 31/12/2009
#' # - a task distribution function of present volumes and maximum usable volume replenishment
#' times from the start of time steps
#' vgest_run_store(objectives[1,],
#'                 1, 1, "Q_NAT_1900-2009.txt",
#'                 "01/01/1900", "31/12/2009", 2)
#'
#' # Example with `vgest_run_store.character`
#' # Running vgest for:
#' # - the first configuration of reservoir rules
#' # - the first configuration of network
#' # - the naturalised hydrological flows of the file located in DONNEES/Q_NAT_1900-2009.txt
#' # - doing the optimisation on the period between 01/01/1900 and 31/12/2009
#' # - an objective located at "PARIS_05"
#' # - for a flood threshold of 800 m3/s
#' # - a fixed task distribution between reservoirs with a relative change of -20%, -10%, +50%, -30%
#' vgest_run_store("PARIS_05", 1, 1, "Q_NAT_1900-2009.txt", "01/01/1900", "31/12/2009",
#'                   TRUE, 800, 1, c(-0.2, -0.1, 0.5, -0.3))
#' }
vgest_run_store.Objectives <-
  function(x,
           reservoirRuleSet,
           networkSet,
           Qfile ="Q_OBS_OBS.txt",
           startDate,
           endDate,
           distributionType,
           distributionOption = NULL,
           ...) {
    nothing <- apply(x, 1, function(y) {
      vgest_run_store(
        y$station,
        reservoirRuleSet,
        networkSet,
        Qfile,
        startDate,
        endDate,
        y$flood,
        y$threshold,
        distributionType,
        distributionOption = NULL,
        ...
      )
    })
  }
