#' Write configuration files for running VGEST
#'
#' Write CHOIX.TXT, objective file and eventually REGLAGES.TXT in PARAMETR folder of VGEST.
#'
#' @details
#' The format of the PARAMETR/CHOIX.TXT file is as follow:
#'
#' - line 1: name of the station where the flow target is located
#' - line 2: rank of the set of parameters describing the reservoirs and their management rules (constraints and local instructions)
#' - line 3: rank of the set of parameters describing the network
#' - line 4: name of the flow data file (hydrological regime without reservoirs)
#' - line 5: name of the file describing the annual objective hydrograph at the bottom station of the system
#' - line 6: objective type: 0 for low flow support, 1 for high flow lamination
#' - line 7: start of calculation period
#' - line 8: end of calculation period
#' - line 9: code indicating which sub programs should be run:
#'   - 0: All sub programs
#'   - 1: Only backward simulation (detailed outputs in PaChrono.txt)
#'   - 2: Only backward simulation (simplified outputs in PaChrVob.txt)
#' - line 10: code for output format of volume results: 1 for absolute values in m3 , 2 for relative values with respect to Vtot or sum(Vtot)
#' - line 11: code indicating how the flow to be stored is distributed between the reservoirs:
#'   - 1=fixed;
#'   - 2=function of present volumes and maximum usable volume replenishment times from the start of time steps;
#'   - 3=aimed at balancing the filling rates at the end of the time step;
#'   - 4=aiming to balance at the end of the time step the times Tpot of reservoir evolution towards extreme state (see line 11) with average inputs;
#'   - 5=aiming to balance at the end of the time step the times Tpot of evolution of the reservoir towards extreme state (see line 11) with given quantity of the contributions of each quantity.
#' - line 12 (used only if 4 or 5 on line 11): code indicating the nature of Tpot :
#'   - 1: Tpot is the minimum potential duration Tpot1 of reconstitution of the maximum usable volume (obtaining V=Vtot or V=0, depending on the nature of the objective (support or rolling) and the direction of the calculations)
#'   - 2: Tpot is the minimum potential duration Tpot2 of exhaustion of the usable volume (obtaining V=Vtot or V=0, depending on the nature of the objective (support or rolling) and the direction of the calculations)
#'
#' @param reservoirRuleSet rank of the set of parameters describing the reservoirs
#' and their management rules (constraints and local instructions)
#' @param networkSet rank of the set of parameters describing the network
#' @param station name of the station where the flow target is located
#' @param Qfile name of the flow data file (hydrological regime without reservoirs)
#' @param bFlood boolean describing the objective type: \code{FALSE} for low flow
#' support, \code{TRUE} for high flow lamination
#' @param startDate start of calculation period in "dd/mm/yyyy" format
#' @param endDate end of calculation period in "dd/mm/yyyy" format
#' @param threshold value of the threshold in m3/s
#' @param distributionType see in details the description of line 10 of CHOIX.TXT
#' @param distributionOption for \code{distributionType=1}, this should contains
#' the relative change in comparison with a repartition based on storage capacity
#' of fixed repartition between reservoirs. If \code{distributionType=4} or \code{5},
#' see in details the description of line 11 of CHOIX.TXT (default \code{NULL})
#' @param vgest_path location of the vgest software (default "../vgest")
#' @param objective_file name of the file used for storing the threshold hydrograph
#' @param formatResult code for output format of volume results: 1 for absolute values
#' in m3 , 2 for relative values with respect to Vtot or sum(Vtot)
#' @param subPrograms [integer] 0 for running all sub programs, 1 for running
#' only backward simulation with detailed results, 2 for running only backward
#' simulation (default value)
#'
#' @return Function used for side effect.
#' @export
#'
#' @examples
#' \dontrun{
#' # Preparing the run of vgest for:
#' # - the first configuration of reservoir rules
#' # - the first configuration of network
#' # - the naturalised hydrological flows of the file located in DONNEES/Q_NAT_1900-2009.txt
#' # - doing the optimisation on the period between 01/01/1900 and 31/12/2009
#' # - an objective located at "PARIS_05"
#' # - for a flood threshold of 800 m3/s
#' # - a fixed task distribution between reservoirs with a relative change of -20%, -10%, +50%, -30%
#' vgest_write_batch(1, 1, "Q_NAT_1900-2009.txt", "01/01/1900", "31/12/2009",
#'                   "PARIS_05", TRUE, 800, 1, c(-0.2, -0.1, 0.5, -0.3))
#' }
vgest_write_batch <- function(reservoirRuleSet, networkSet,
                              Qfile, startDate, endDate,
                              station, bFlood, threshold,
                              distributionType, distributionOption = NULL,
                              vgest_path = file.path(tempdir(), "vgest"),
                              objective_file = "BATCH",
                              formatResult = 1,
                              subPrograms = 2) {

  if(distributionType %in% c(1, 4, 5) & is.null(distributionOption)) {
    stop("distribution option should be defined for distributionType = 1, 4 or 5")
  }
  sMode <- as.numeric(bFlood) # 0 for drought, 1 for flood
  s <- c(station, reservoirRuleSet, networkSet,
         Qfile, objective_file, sMode, startDate, endDate,
         subPrograms, formatResult, distributionType)
  if(distributionType == 1) {
    writeLines(as.character(distributionOption), file.path(vgest_path, "PARAMETR", "REGLAGE.txt"))
  } else if(distributionType %in% c(4,5)) {
    s <- c(s,distributionOption)
  }
  s <- as.character(s)

  writeLines(s, file.path(vgest_path, "PARAMETR", "CHOIX.txt"))
  writeLines(paste("01/01", threshold), file.path(vgest_path, "OBJECTIF", objective_file))
}
