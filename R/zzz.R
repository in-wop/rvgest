.onLoad <- function(libname, pkgname){
  dir = Sys.getenv("PKG_DATA_CACHE",
                   file.path(dirname(tempdir()), packageName(), "cachem"))
  dir.create(dir, showWarnings = FALSE, recursive = TRUE)
}