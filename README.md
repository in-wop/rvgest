# RVGEST

R package wrapper for VGEST

## Installation

### Requirements

The third party program 'VGEST' is required to perform the optimisations. Clone or download 'VGEST' sources and install it at the same folder level as the rvgest folder ('rvgest' and 'vgest' folders should be located in the same folder). Follow the instruction at https://gitlab.irstea.fr/in-wop/vgest for installation and compilation. Then, prepare the model for the Seine Basin case by following the steps of the "get started" example.

Finally, copy a naturalised flow database in the `vgest/DONNEES` folder.

### Installation

From INRAE Gitlab repository:

```r
install.packages("remotes")
remotes::install_gitlab("in-wop/rvgest@master", host = "gitlab.irstea.fr")
```

## Usage

### Running VGEST and plot outputs

See the dedicated vignette "run_vgest_basics" in the `vignettes` folder of the package.

### Documentation

See all available functions and their documentation by typing `help(package = "rvgest")` in a R console.

## Code of Conduct

Please note that the rvgest project is released with a [Contributor Code of Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html). By contributing to this project, you agree to abide by its terms.