## code to prepare `seine_grands_lacs` dataset goes here


# *** lakes ***
lakes <- read.delim("data-raw/lakes.txt")
row.names(lakes) <- lakes$name

# *** objectives ***
thresholds <- read.delim("data-raw/objective_thresholds.txt")
stations <- jsonlite::fromJSON(
  readLines("data-raw/objective_stations.json")
)
bFirst = TRUE
for(iStation in 1:nrow(thresholds)) {
  station <- thresholds[iStation, 1]
  for(type in c("l", "h")) {
    bFlood = (type == "h")
    for(level in grep(paste0(type, ".*"), names(thresholds))) {
      threshold <- thresholds[iStation, level]
      df1 <- data.frame(
        station = station,
        flood = bFlood,
        level = names(thresholds)[level],
        threshold = threshold
      )
      df1$lakes <- list(lakes[stations[[station]]$lakes,])
      if(bFirst) {
        objectives <- df1
        bFirst <- FALSE
      } else {
        objectives <- rbind(objectives, df1)
      }
    }
  }
}
class(objectives) <- c("Objectives", class(objectives))

# *** rulesets ***
constraints <- c(
  "a. Qres (minimum flow) at inlets and at Yonne outlet (inline reservoir)",
  "b. Qres at outlets",
  "c. Qref (maximum flow) at inlets and outlets",
  "d. Priority to hydropower generation on Yonne reservoir (Max outflow of 16m<sup>3</sup>/s)",
  "e. QST Gradient flow daily limitation for Yonne reservoir (+/- 2m3/s per day)"
)

rules <- c(
  "All constraints (a+b+c+d+e)",
  "1, without Qref (a+b+d+e)",
  "1, without Qres at outlet (a+c+d+e)",
  "3, without Qref (a+d+e)",
  "4, without hydropower priority for Yonne lake (a+e)",
  "5, without Yonne outflow variation limitation (a)"
)
rulesets <- list(constraints = constraints, rules = rules)

# Record data in the package
usethis::use_data(objectives, lakes, rulesets, internal = TRUE, overwrite = TRUE)
