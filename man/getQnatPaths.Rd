% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getQnatPaths.R
\name{getQnatPaths}
\alias{getQnatPaths}
\title{Get the paths of the files tsQsim (historical and rcp)}
\usage{
getQnatPaths(cfg = rvgest::loadConfig(), rcp = names(cfg$drias$rcp))
}
\arguments{
\item{cfg}{a config object. Configuration to use. See \link{loadConfig} for details}

\item{rcp}{historical or rcp45 and rcp85}

\item{cfgSB2}{Configuration of package seinebasin2}
}
\value{
paths of all scenarios
}
\description{
Get the paths of the files tsQsim (historical and rcp)
}
