test_that("vgest_result_path works", {
  one_objective <- objectives[1, ]
  expect_type(vgest_result_path(one_objective), "character")
})
