---
title: "Instant risk overview"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Instant risk overview}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
cfg <- rvgest::loadConfig("")
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.width = 8,
  fig.asp = 0.8,
  out.width = "70%",
  fig.align = "center"
)
```

```{r}
library(rvgest)
library(dplyr)
```

The risk database provides probabilities of risk which can be used to display an
overview of the risk considering the day of the year and the volume stored in the
reservoirs.

```{r initDB}
cfg <- rvgest::loadConfig("")
cfg$database$mode <- "test"
con <- initDB(cfg = cfg)
```

We choose an arbitrary ruleset for this example among 6 possibilities of rulesets:

```{r ruleset, results='asis'}
id_ruleset <- 1
cat("Ruleset: ", names(rulesets$rules)[id_ruleset], "\n\n")
cat("List of constraints: ", paste("-", rulesets$constraints[rulesets$rules[[id_ruleset]]], collapse = "\n"), "\n", sep = "\n")
```

```{r config}
paramClimateD <- rvgest::getDefaultClimate()
id_rcps <- paramClimateD$id_rcps
id_periods <- paramClimateD$id_periods
```

The risk is calculated on the time period 
`r lubridate::year(getPeriods()$start[id_periods[1]])`-
`r lubridate::year(getPeriods()$end[dplyr::last(id_periods)])` over historical 
hydrology and simulated hydrology forced by GCM/RCM scenarios.

Let's compute the risk overview for the current date:

```{r}
date <- Sys.Date()
date
```

For an arbitrary volume storage of half capacity of each reservoir

```{r}
storages <- lakes$min + (lakes$max - lakes$min) / 2
names(storages) <- lakes$name
storages
```

The instant risk consist the to compute the probability of risk considering all 
these elements (day of the year, volume stored, source of data, period and rule set)
for each objective and to sort the objective by decreasing risk:

```{r}
plotInstantRisk(con, id_ruleset, date, id_rcps, id_periods, storages)
```

As the "test" database used here, only contains objectives calculated for 
Paris-Austerlitz, this plot only shows the objectives related to this station.
